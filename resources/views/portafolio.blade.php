@extends('./theme/layout')

@section('title', ' - Portafolio')

@section('content')

<h1>Portafolio</h1>

<ul>
  @forelse ( $portafolios as $portafolio)
    <li>{{ $portafolio['title'] }}</li>
  @empty
    <li>No hay proyectos para mostrar.</li>
  @endforelse
</ul>

@endsection
