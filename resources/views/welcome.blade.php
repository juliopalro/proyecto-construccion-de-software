<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Laravel</title>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  </head>

  <body>
    <div class="wrapper">

      <!-- menú left -->
      <nav id="sidebar" class="sidebar js-sidebar">
          <div class="sidebar-content js-simplebar">
            <a class="sidebar-brand" href="index.html">
              <span class="align-middle">Juliopalro</span>
            </a>

            <ul class="sidebar-nav">
              <li class="sidebar-header">Páginas</li>
              <li class="sidebar-item active">
                <a class="sidebar-link" href="index.html">
                  <i class="align-middle" data-feather="sliders"></i> <span class="align-middle">Inicio</span>
                </a>
              </li>
              <li class="sidebar-item">
                <a class="sidebar-link" href="pages-profile.html">
                  <i class="align-middle" data-feather="user"></i> <span class="align-middle">Profile</span>
                </a>
              </li>
              <li class="sidebar-item">
                <a class="sidebar-link" href="pages-sign-in.html">
                  <i class="align-middle" data-feather="log-in"></i> <span class="align-middle">Sign In</span>
                </a>
              </li>
              <li class="sidebar-item">
                <a class="sidebar-link" href="pages-sign-up.html">
                  <i class="align-middle" data-feather="user-plus"></i> <span class="align-middle">Sign Up</span>
                </a>
              </li>
              <li class="sidebar-item">
                <a class="sidebar-link" href="pages-blank.html">
                  <i class="align-middle" data-feather="book"></i> <span class="align-middle">Blank</span>
                </a>
              </li>
            </ul>
          </div>
      </nav>

      <div class="main">
        <!-- Menú top -->
        <nav class="navbar navbar-expand navbar-light navbar-bg">
          <a class="sidebar-toggle js-sidebar-toggle">
            <i class="hamburger align-self-center"></i>
          </a>

          <div class="navbar-collapse collapse">
            <ul class="navbar-nav navbar-align">
              <!-- Profile -->
              <li class="nav-item dropdown">
                <a class="nav-icon pe-md-0 dropdown-toggle" href="#" data-bs-toggle="dropdown" aria-expanded="false">
                  <div class="avatar img-fluid rounded me-1" alt="Charles Hall">
                    <i class="align-middle" data-feather="user"></i>
                  </div>
                </a>

                <div class="dropdown-menu dropdown-menu-end">
                  <a class="dropdown-item" href="pages-profile.html"><i class="align-middle me-1" data-feather="user"></i> Profile</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="#">Log out</a>
                </div>
              </li>
            </ul>
          </div>
        </nav>

        <!-- Cuerpo de la páina-->
        <main class="content">
          <div class="container-fluid p-0">
          </div>
        </main>

        <!-- Pié de página -->
        <footer class="footer">
          <div class="container-fluid">
            <div class="row text-muted">
              <div class="col-6 text-start">
                <p class="mb-0">
                  <a class="text-muted" href="https://adminkit.io/" target="_blank"><strong>AdminKit</strong></a> - <a class="text-muted" href="https://adminkit.io/" target="_blank"><strong>Bootstrap Admin Template</strong></a>								&copy;
                </p>
              </div>
              <div class="col-6 text-end">
                <ul class="list-inline">
                  <li class="list-inline-item">
                    <a class="text-muted" href="https://adminkit.io/" target="_blank">Support</a>
                  </li>
                  <li class="list-inline-item">
                    <a class="text-muted" href="https://adminkit.io/" target="_blank">Help Center</a>
                  </li>
                  <li class="list-inline-item">
                    <a class="text-muted" href="https://adminkit.io/" target="_blank">Privacy</a>
                  </li>
                  <li class="list-inline-item">
                    <a class="text-muted" href="https://adminkit.io/" target="_blank">Terms</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </footer>
      </div>
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

  </body>
</html>
