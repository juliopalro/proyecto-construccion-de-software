@extends('./theme/layout')

@section('title', ' - Examen parcial')

@section('content')
  <h1>Agregar contacto</h1>

  <form class="row" method="POST" action="{{ route('form.store') }}">
    @csrf
    <div class="mb-3">
      <label for="full_name" class="col-form-label">Nombre completo</label>
      <input type="text" class="form-control @error('full_name') is-invalid @enderror" id="full_name" name="full_name"
        placeholder="Pablito Calvo" value="{{ old('full_name') }}">
      @error('full_name')
        <div class="invalid-feedback">{{ $message }}</div>
      @enderror
    </div>
    <div class="mb-3">
      <label for="email" class="col-form-label">Email</label>
      <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email"
        placeholder="ejemplo@gmail.com" value="{{ old('email') }}">
      @error('email')
        <div class="invalid-feedback">{{ $message }}</div>
      @enderror
    </div>
    <div class="mb-3">
      <label for="cell_phone" class="col-form-label">Celular</label>
      <input type="text" class="form-control @error('cell_phone') is-invalid @enderror" id="cell_phone" name="cell_phone"
        placeholder="954862358" value="{{ old('cell_phone') }}">
      @error('cell_phone')
        <div class="invalid-feedback">{{ $message }}</div>
      @enderror
    </div>
    <div class="mb-3">
      <label for="address" class="col-form-label">Dirección</label>
      <input type="text" class="form-control @error('address') is-invalid @enderror" id="address" name="address"
        placeholder="Av. Ejemplo 245 - Huancayo" value="{{ old('address') }}">
      @error('address')
        <div class="invalid-feedback">{{ $message }}</div>
      @enderror
    </div>

    <div class="col-auto">
      <button type="submit" class="btn btn-primary mb-3">Guardar</button>
    </div>
  </form>
@endsection
