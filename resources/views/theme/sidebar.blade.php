<!-- menú left -->
<nav id="sidebar" class="sidebar js-sidebar">
  <div class="sidebar-content js-simplebar">
    <a class="sidebar-brand" href="{{ route('home') }}">
      <span class="align-middle">Juliopalro</span>
    </a>

    <ul class="sidebar-nav">
      <li class="sidebar-header">Páginas</li>
      <li class="sidebar-item {{ isRouteActiveName('home') }}">
        <a class="sidebar-link" href="{{ route('home') }}">
          <i class="align-middle" data-feather="home"></i> <span class="align-middle">{{ __('Home')}}</span>
        </a>
      </li>
      <li class="sidebar-item {{ isRouteActiveName('about') }}">
        <a class="sidebar-link" href="{{ route('about') }}">
          <i class="align-middle" data-feather="user"></i> <span class="align-middle">{{ __('About')}}</span>
        </a>
      </li>
      <li class="sidebar-item {{ isRouteActiveName('portafolio') }}">
        <a class="sidebar-link" href="{{ route('portafolio') }}">
          <i class="align-middle" data-feather="folder"></i> <span class="align-middle">{{ __('Portfolio')}}</span>
        </a>
      </li>
      <li class="sidebar-item {{ isRouteActiveName('contact') }}">
        <a class="sidebar-link" href="{{ route('contact') }}">
          <i class="align-middle" data-feather="edit"></i> <span class="align-middle">{{ __('Contact')}}</span>
        </a>
      </li>
      <li class="sidebar-item {{ isRouteActiveName('description') }}">
        <a class="sidebar-link" href="{{ route('description') }}">
          <i class="align-middle" data-feather="align-center"></i> <span class="align-middle">{{ __('Description')}}</span>
        </a>
      </li>
      <li class="sidebar-item {{ isRouteActiveName('form.index') }}">
        <a class="sidebar-link" href="{{ route('form.index') }}">
          <i class="align-middle" data-feather="align-center"></i> <span class="align-middle">Examen parcial</span>
        </a>
      </li>
    </ul>
  </div>
</nav>
