<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>juliopalro @yield('title')</title>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  </head>

  <body>
    <div class="wrapper">
      @include('theme.sidebar')

      <div class="main">
        @include('theme.navbar')

        <!-- Cuerpo de la página-->
        <main class="content">
          <div class="container-fluid p-0">
            @yield('content')
          </div>
        </main>

        @include('theme.footer')
      </div>
    </div>

    <!-- Script -->
    <script src="{{ asset('js/app.js') }}" defer></script>
  </body>
</html>
