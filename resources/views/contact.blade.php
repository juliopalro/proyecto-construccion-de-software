@extends('./theme/layout')

@section('title', ' - Contact')

@section('content')
  <h1>{{ __('Contact')}}</h1>

  <form class="row" method="POST" action="{{ route('contact') }}">
    @csrf
    <div class="mb-3">
      <label for="full_name" class="col-form-label">Nombre completo</label>
      <input type="text" class="form-control @error('full_name') is-invalid @enderror" id="full_name" name="full_name"
        placeholder="Pablito Calvo" value="{{ old('full_name') }}">
      @error('full_name')
        <div class="invalid-feedback">{{ $message }}</div>
      @enderror
    </div>
    <div class="mb-3">
      <label for="email" class="col-form-label">Email</label>
      <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email"
        placeholder="example@gmail.com" value="{{ old('email') }}">
      @error('email')
        <div class="invalid-feedback">{{ $message }}</div>
      @enderror
    </div>
    <div class="mb-3">
      <label for="subject" class="col-form-label">Asunto</label>
      <input type="text" class="form-control @error('subject') is-invalid @enderror" id="subject" name="subject"
        placeholder="..." value="{{ old('subject') }}">
      @error('subject')
        <div class="invalid-feedback">{{ $message }}</div>
      @enderror
    </div>
    <div class="mb-3">
      <label for="content" class="col-form-label">Mensaje</label><br>
      <textarea class="form-control @error('content') is-invalid @enderror" name="content" id="content" rows="5">{{ old('content') }}</textarea>
      @error('content')
        <div class="invalid-feedback">{{ $message }}</div>
      @enderror
    </div>

    <div class="col-auto">
      <button type="submit" class="btn btn-primary mb-3">Enviar</button>
    </div>
  </form>
@endsection
