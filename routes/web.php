<?php

use App\Http\Controllers\FormController;
use App\Http\Controllers\MsgController;
use App\Http\Controllers\PortafolioController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::view('/home', 'home')->name('home'); // Inicio
Route::view('/about', 'about')->name('about'); // Sobre mi
Route::get('/portafolio', PortafolioController::class)->name('portafolio'); // Portafolio
Route::view('/contact', 'contact')->name('contact'); // Contacto
Route::post('/contact', [MsgController::class, 'store']);
Route::view('/description', 'description')->name('description'); // Descripción
// Route::get('/ejercicio', 'ProyectoController')->name('ejercicio'); // Ejercicio de prueba

Route::resource('/form', FormController::class)->only(['index', 'store']);
