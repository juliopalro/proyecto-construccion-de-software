<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function index()
    {
        return view('midterm-exam.index');
    }

    public function store(Request $request)
    {
        $request->validate([
            'full_name' => 'required',
            'email' => 'required|email',
            'cell_phone' => 'required|numeric|min:9',
            'address' => 'required|numeric|min:9'
        ],
        [
            'full_name.required' => 'Necesitamos tu nombre completo para dirigirnos contigo',
            'email.required' => 'Necesitamos tu correo para poder contactarte',
            'cell_phone.required' => 'Agrega tu celular para poder comunicarnos contigo'
        ]);

        return view('midterm-exam.index', compact($request));
    }

}
