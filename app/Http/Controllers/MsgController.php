<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MsgController extends Controller
{
    public function store(Request $request)
    {
        $request->validate([
            'full_name' => 'required',
            'email' => 'required|email',
            'subject' => 'required',
            'content' => 'required|min:3'
        ],
        [
            'full_name.required' => 'Necesitamos tu nombre completo para dirigirnos contigo',
            'email.required' => 'Necesitamos tu correo para poder contactarte'
        ]);

        return "Datos validados";
    }
}
