<?php

/**
 * Devuelve active en caso el nombre de la ruta sea la enviada para activar la clase
 */
function isRouteActiveName($routeName)
{
  return request()->routeIs($routeName) ? 'active' : '';
};
